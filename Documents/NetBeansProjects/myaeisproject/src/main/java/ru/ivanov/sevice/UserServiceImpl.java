package ru.ivanov.sevice;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import ru.ivanov.dao.UserDao;
import ru.ivanov.model.User;

@Stateless
public class UserServiceImpl implements UserService {

    @EJB
    private UserDao userDao;

    @Override
    public User createUser(User user) {
        return userDao.create(user);
    }

    @Override
    public User editUser(User user) {
        return userDao.update(user);
    }

    @Override
    public void deleteUser(User user) {
        userDao.remove(user);
    }

    @Override
    public User getUserByEmail(String email) {
        return userDao.findById(email);
    }
}
