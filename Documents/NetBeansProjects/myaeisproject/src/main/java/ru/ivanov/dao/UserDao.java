package ru.ivanov.dao;

import javax.ejb.Stateless;
import ru.ivanov.model.User;

@Stateless
public class UserDao extends GenericDaoJpa<User, String>{

    public UserDao() {
        super(User.class);
    }
}
